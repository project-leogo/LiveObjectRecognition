package com.LOGO.logo.liveobjectrecognition.app;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by andi on 29/11/15.
 */
public class DetectObject
{

    public Mat detectAndDisplay( Mat frame, Context context )
    {

        CascadeClassifier face_cascade = new CascadeClassifier();
        CascadeClassifier eyes_cascade= new CascadeClassifier();
        Activity activity = ((Activity)context);

        try{
            InputStream is = activity.getResources().openRawResource(R.raw.haarcascade_frontalface_alt);
            File cascadeDir = activity.getDir("cascade", Context.MODE_PRIVATE);
            File mCascadeFile = new File(cascadeDir,"new_xml.xml");
            FileOutputStream os = new FileOutputStream(mCascadeFile);

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();

            face_cascade = new CascadeClassifier(mCascadeFile.getAbsolutePath());
            if(face_cascade.empty())
            {
                Log.v("MyActivity", "--(!)Error loading A\n");
            }
            else
            {
                Log.v("MyActivity",
                        "Loaded cascade classifier from " + mCascadeFile.getAbsolutePath());
            }
        }catch (IOException e){
            Log.e(DetectObject.class.toString(), "Io Exception occured while trying to open .xml file", e);
        }




        try{
            InputStream is = activity.getResources().openRawResource(R.raw.haarcascade_eye_tree_eyeglasses);
            File cascadeDir = activity.getDir("cascade", Context.MODE_PRIVATE);
            File mCascadeFile = new File(cascadeDir,"new_xml_eye.xml");
            FileOutputStream os = new FileOutputStream(mCascadeFile);

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();

            eyes_cascade = new CascadeClassifier(mCascadeFile.getAbsolutePath());
            if(eyes_cascade.empty())
            {
                Log.v("MyActivity", "--(!)Error loading A\n");
            }
            else
            {
                Log.v("MyActivity",
                        "Loaded cascade classifier from " + mCascadeFile.getAbsolutePath());
            }
        }catch (IOException e){
            Log.e(DetectObject.class.toString(), "Io Exception occured while trying to open .xml file", e);
        }












        if(face_cascade.empty())
        {
            Log.e("232323", "2323");
        }


        MatOfRect faces1 = new MatOfRect();
        Mat frame_gray = new Mat();

        Imgproc.cvtColor(frame, frame_gray, Imgproc.COLOR_BGR2GRAY);
        Imgproc.equalizeHist(frame_gray, frame_gray);


        face_cascade.detectMultiScale(frame_gray, faces1);
        List<Rect> faces = new LinkedList<Rect>(Arrays.asList(faces1.toArray()));

        for( int i = 0; i < faces.size(); i++ )
        {
            Point center=new Point( faces.get(i).x + faces.get(i).width*0.5, faces.get(i).y + faces.get(i).height*0.5 );
            Core.ellipse(frame, center, new Size(faces.get(i).width * 0.5, faces.get(i).height * 0.5), 0, 0, 360, new Scalar(255, 0, 255), 4, 8, 0);

            Mat faceROI = frame_gray.submat(faces.get(i)); //frame_gray( faces.get(i) );
            MatOfRect eyes1 =new MatOfRect();

            //-- In each face, detect eyes
            //eyes_cascade.detectMultiScale( faceROI, eyes1, 1.1, 2, 0 |Objdetect.CASCADE_SCALE_IMAGE, new Size(30, 30) , new Size(500,500));
            eyes_cascade.detectMultiScale(frame_gray.submat(faces.get(i)), eyes1);

            List<Rect> eyes = eyes1.toList();//weiß nicht, ob das so geht

            for( int j = 0; j < eyes.size(); j++ )
            {
                center = new Point( faces.get(i).x + eyes.get(j).x + eyes.get(j).width*0.5, faces.get(i).y + eyes.get(j).y + eyes.get(j).height*0.5 );
                int radius = (int) Math.round((eyes.get(j).width + eyes.get(j).height) * 0.25);// cvRound( (eyes.get(j).width + eyes.get(j).height)*0.25 );
                Core.circle( frame, center, radius, new Scalar( 255, 0, 0 ), 4, 8, 0 );
            }
        }



        return frame;
    }





}
